package com.example.parqour.model;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class UrlCodeModel {

    private Long id;
    private String code;
    private String url;
    private String shortUrl;
    private String cutUrl;
    private Integer countCall;
    private LocalDateTime createDate;

}
