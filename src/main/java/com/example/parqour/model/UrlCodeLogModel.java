package com.example.parqour.model;

import com.example.parqour.enums.LogType;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class UrlCodeLogModel {
    private Long id;
    private UrlCodeModel urlCode;
    private LocalDateTime createDate;
    private LogType type;
}
