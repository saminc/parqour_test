package com.example.parqour.property;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:application.properties")
@Data
public class Property {

    @Value("${url.limit}")
    private Integer limit;

    @Value("${url.code.host}")
    private String hostName;

    @Value("${url.code.chars}")
    private String chars;

    @Value("${url.code.error_msg}")
    private String codeErrorMsg;
}
