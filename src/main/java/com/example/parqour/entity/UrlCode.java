package com.example.parqour.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table (name = "url_code")
@Data
public class UrlCode {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column (name = "code")
    private String code;

    @Column (name = "url")
    private String url;

    @Column (name = "countCall")
    private Integer countCall = 0;

    @Column (name = "create_date")
    private LocalDateTime createDate = LocalDateTime.now();
}
