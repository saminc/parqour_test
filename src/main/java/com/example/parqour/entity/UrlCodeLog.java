package com.example.parqour.entity;


import com.example.parqour.enums.LogType;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "url_code_log")
@Data
public class UrlCodeLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "url_code_id")
    private UrlCode urlCode;

    @Column(name = "create_date")
    private LocalDateTime createDate = LocalDateTime.now();

    @Enumerated(EnumType.STRING)
    @Column (name = "log_type")
    private LogType type;
}
