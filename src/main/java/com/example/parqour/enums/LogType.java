package com.example.parqour.enums;

public enum LogType {
    CODE("Код"), LONG_URL("URL-адрес");

    private final String type;

    LogType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
