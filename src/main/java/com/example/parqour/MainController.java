package com.example.parqour;

import com.example.parqour.model.UrlCodeLogModel;
import com.example.parqour.model.UrlCodeModel;
import com.example.parqour.property.Property;
import com.example.parqour.service.ParqourTestService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.Collections;
import java.util.List;


@Controller
@RequiredArgsConstructor
public class MainController {
    private final ParqourTestService service;
    private final Property property;



    @GetMapping("/")
    public String indexPage(Model model) {
        return "index";
    }

    @GetMapping("/log")
    public String logPage(Model model) {
        List<UrlCodeModel> list = service.getAllCode();
        if (list == null) {
            model.addAttribute("list", Collections.EMPTY_LIST);
        } else {
            model.addAttribute("list", list);
        }
        return "log";
    }


    @GetMapping("/code/log/info/{urlCodeId}")
    public String logInfoPage(Model model, @PathVariable(required = false) Long urlCodeId) {
        List<UrlCodeLogModel> list = service.getLogByUrlCode(urlCodeId);
        UrlCodeModel urlCodeModel = service.getUrlCodeById(urlCodeId);
        model.addAttribute("urlCodeModel", urlCodeModel);
        if (list == null) {
            model.addAttribute("list", Collections.EMPTY_LIST);
        } else {
            model.addAttribute("list", list);

        }
        return "logInfo";
    }


    @PostMapping("/send_url")
    public String sendUrl(RedirectAttributes attributes,
                          @RequestParam(required = false, name = "url") String url) {
        UrlCodeModel model = service.saveUrlCode(url);
        if (!ObjectUtils.isEmpty(model) && !ObjectUtils.isEmpty(model.getCode())) {
            attributes.addFlashAttribute("msg_code", "Код: " + model.getCode());
            attributes.addFlashAttribute("shortUrlText", " URL: " + model.getShortUrl());
            attributes.addFlashAttribute("shortUrl", model.getShortUrl());
        } else {
            attributes.addFlashAttribute("msg_ERROR", "Ошибка генерации кода, код пуст");
        }
        return "redirect:/";
    }


    @PostMapping("/send_code")
    public String sendCode(RedirectAttributes attributes,
                           @RequestParam(required = false, name = "code") String code) {
        UrlCodeModel model = service.getUrlCodeByCode(code);
        if (!ObjectUtils.isEmpty(model) && !ObjectUtils.isEmpty(model.getUrl())) {
            return "redirect:" + model.getUrl();
        } else {
            attributes.addFlashAttribute("msg_ERROR", "Url не найден");
            return "redirect:/";
        }
    }


    @GetMapping("/{code}")
    public String getCode(@PathVariable String code) throws Exception {
        UrlCodeModel model = service.getUrlCodeByCode(code);
        if (!ObjectUtils.isEmpty(model) && !ObjectUtils.isEmpty(model.getUrl())) {

            return "redirect:" + model.getUrl();
        } else {
            throw new HttpServerErrorException(HttpStatus.INTERNAL_SERVER_ERROR, property.getCodeErrorMsg());
        }
    }


    @ExceptionHandler(HttpServerErrorException.class)
    ResponseEntity<String> handleHttpServerError(final HttpServerErrorException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
