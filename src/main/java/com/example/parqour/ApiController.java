package com.example.parqour;

import com.example.parqour.service.ParqourTestService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class ApiController {

    private final ParqourTestService service;

    @RequestMapping(method = RequestMethod.POST, value = "/shrink")
    public ResponseEntity<?> shrinkUrl(String url, ModelMapper modelMapper) throws Exception {
        return service.saveUrlCodeResponse(url, modelMapper);
    }




}
