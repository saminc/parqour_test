package com.example.parqour.service;

import com.example.parqour.entity.UrlCode;
import com.example.parqour.entity.UrlCodeLog;
import com.example.parqour.model.UrlCodeLogModel;
import com.example.parqour.model.UrlCodeModel;
import com.example.parqour.response.GenericResponse;
import com.example.parqour.response.UrlCodeResponse;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ParqourTestService {

    UrlCodeModel saveUrlCode(String url);
    UrlCodeModel getUrlCodeByCode(String code);
    UrlCodeModel getUrlCodeById(Long urlCodeId);
    List<UrlCodeModel> getAllCode();
    List<UrlCodeLogModel> getLogByUrlCode(Long urlCodeId);

    ResponseEntity<?> saveUrlCodeResponse(String url, ModelMapper modelMapper);


}
