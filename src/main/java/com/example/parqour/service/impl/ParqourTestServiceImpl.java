package com.example.parqour.service.impl;

import com.example.parqour.entity.UrlCode;
import com.example.parqour.entity.UrlCodeLog;
import com.example.parqour.enums.LogType;
import com.example.parqour.model.UrlCodeLogModel;
import com.example.parqour.model.UrlCodeModel;
import com.example.parqour.property.Property;
import com.example.parqour.repo.UrlCodeLogRepository;
import com.example.parqour.repo.UrlCodeRepository;
import com.example.parqour.response.GenericResponse;
import com.example.parqour.response.UrlCodeResponse;
import com.example.parqour.service.ParqourTestService;
import com.example.parqour.service.UrlCodeMapperService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ParqourTestServiceImpl implements ParqourTestService {

    private final UrlCodeRepository urlCodeRepository;
    private final UrlCodeLogRepository urlCodeLogRepository;
    private final UrlCodeMapperService mapperService;
    private final Property property;


    @Override
    public UrlCodeModel saveUrlCode(String url) {
        UrlCode urlCode = getUrlCodeByUrl(url);
        if (ObjectUtils.isEmpty(urlCode)) {
            urlCode = new UrlCode();
            urlCode.setCode(getCode());
            urlCode.setUrl(url);
            urlCodeRepository.save(urlCode);
        }
        return mapperService.entityToModel(urlCode);
    }

    private UrlCode getUrlCodeByUrl(String url) {
        UrlCode urlCode = urlCodeRepository.findByUrl(url);
        if (!ObjectUtils.isEmpty(urlCode)) {
            createLog(urlCode, LogType.LONG_URL);
        }
        return urlCode;
    }

    @Override
    public UrlCodeModel getUrlCodeByCode(String code) {
        UrlCode urlCode = urlCodeRepository.findByCode(code);
        if (!ObjectUtils.isEmpty(urlCode)) {
            createLog(urlCode, LogType.CODE);
        }
        return mapperService.entityToModel(urlCode);
    }

    @Override
    public UrlCodeModel getUrlCodeById(Long urlCodeId) {
        UrlCode urlCode = urlCodeRepository.getById(urlCodeId);
        return mapperService.entityToModel(urlCode);
    }

    private void createLog(UrlCode urlCode, LogType type) {
        if (!ObjectUtils.isEmpty(urlCode)) {
            urlCode.setCountCall(urlCode.getCountCall() + 1);
            urlCodeRepository.save(urlCode);

            UrlCodeLog log = new UrlCodeLog();
            log.setUrlCode(urlCode);
            log.setType(type);
            urlCodeLogRepository.save(log);
        }

    }

    @Override
    public List<UrlCodeModel> getAllCode() {
        List<UrlCode> urlCodes = urlCodeRepository.findAll()
                .stream().sorted(Comparator.comparing(UrlCode::getCountCall).reversed())
                .collect(Collectors.toList());
        return urlCodes.stream().map(mapperService::entityToModel).collect(Collectors.toList());
    }

    @Override
    public List<UrlCodeLogModel> getLogByUrlCode(Long urlCodeId) {
        List<UrlCodeLog> urlCodeLogs = urlCodeLogRepository.findByUrlCodeId(urlCodeId);
        return urlCodeLogs.stream().map(mapperService::entityToModel).collect(Collectors.toList());
    }

    @Override
    public ResponseEntity<?> saveUrlCodeResponse(String url, ModelMapper modelMapper) {
        UrlCodeModel model = saveUrlCode(url);
        if (!ObjectUtils.isEmpty(model) && !ObjectUtils.isEmpty(model.getUrl())) {
            UrlCodeResponse response =  UrlCodeResponse.builder()
                    .code(model.getCode())
                    .shortUrl(property.getHostName().concat(model.getCode())).build();
            return new ResponseEntity<>(
                    new GenericResponse<>(null, null,
                            modelMapper.map(response, UrlCodeResponse.class)

                    ), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(
                    GenericResponse.error(
                            HttpStatus.INTERNAL_SERVER_ERROR.value(),
                            property.getCodeErrorMsg()),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    public String getCode() {
        String code = generateHash6Length();
        while (foundExistCode(code)) {
            code = generateHash6Length();
        }
        return code;
    }

    private boolean foundExistCode(String code) {
        UrlCode urlCode = urlCodeRepository.findByCode(code);
        return !ObjectUtils.isEmpty(urlCode);
    }

    public String generateHash6Length() {
        String allowedChars = property.getChars();
        return RandomStringUtils.random(6, allowedChars);
    }
}
