package com.example.parqour.service.impl;

import com.example.parqour.entity.UrlCode;
import com.example.parqour.entity.UrlCodeLog;
import com.example.parqour.model.UrlCodeLogModel;
import com.example.parqour.model.UrlCodeModel;
import com.example.parqour.property.Property;
import com.example.parqour.service.UrlCodeMapperService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UrlCodeMapperServiceImpl implements UrlCodeMapperService {

    private final Property property;

    @Override
    public UrlCodeModel entityToModel(UrlCode urlCode) {
        String cutUrl;
        if (!ObjectUtils.isEmpty(urlCode) &&
                !ObjectUtils.isEmpty(urlCode.getUrl())) {
            cutUrl = urlCode.getUrl();
            if (urlCode.getUrl().length() > property.getLimit()) {
                cutUrl = urlCode.getUrl().substring(0, property.getLimit()).concat("...");
            }

            return UrlCodeModel.builder()
                    .id(urlCode.getId())
                    .code(urlCode.getCode())
                    .url(urlCode.getUrl())
                    .shortUrl(property.getHostName().concat(urlCode.getCode()))
                    .cutUrl(cutUrl)
                    .countCall(urlCode.getCountCall())
                    .createDate(urlCode.getCreateDate())
                    .build();
        }
        return null;
    }

    @Override
    public UrlCodeLogModel entityToModel(UrlCodeLog urlCodeLog) {
        if (!ObjectUtils.isEmpty(urlCodeLog)) {
            return UrlCodeLogModel.builder()
                    .id(urlCodeLog.getId())
                    .urlCode(entityToModel(urlCodeLog.getUrlCode()))
                    .createDate(urlCodeLog.getCreateDate())
                    .type(urlCodeLog.getType())
                    .build();
        }
        return null;
    }
}
