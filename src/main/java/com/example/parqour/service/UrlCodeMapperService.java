package com.example.parqour.service;

import com.example.parqour.entity.UrlCode;
import com.example.parqour.entity.UrlCodeLog;
import com.example.parqour.model.UrlCodeLogModel;
import com.example.parqour.model.UrlCodeModel;

public interface UrlCodeMapperService {
    UrlCodeModel entityToModel(UrlCode urlCode);
    UrlCodeLogModel entityToModel(UrlCodeLog urlCodeLog);
}
