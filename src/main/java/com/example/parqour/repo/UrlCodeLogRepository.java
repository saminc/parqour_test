package com.example.parqour.repo;

import com.example.parqour.entity.UrlCodeLog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UrlCodeLogRepository extends JpaRepository<UrlCodeLog, Long> {

    List<UrlCodeLog> findByUrlCodeId(Long urlCodeId);
}
