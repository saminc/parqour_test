package com.example.parqour.repo;

import com.example.parqour.entity.UrlCode;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UrlCodeRepository extends JpaRepository<UrlCode, Long> {

    UrlCode findByCode(String code);
    UrlCode findByUrl(String code);
}
