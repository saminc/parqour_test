package com.example.parqour.response;

import lombok.Data;

@Data
@SuppressWarnings("PMD.UnusedPrivateField")
public class GenericResponse<T> {

    private Integer errorCode;
    private String errorMessage;
    private T resultData;

    public GenericResponse() {
    }

    public GenericResponse(Integer errorCode, String errorMessage, T resultData) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.resultData = resultData;
    }

    public static <T> GenericResponse<T> error(Integer errorCode, String errorMessage) {
        return new GenericResponse<>(errorCode, errorMessage, null);
    }
}
