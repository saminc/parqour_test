package com.example.parqour.response;


import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Builder
@Data
public class UrlCodeResponse implements Serializable {
    private String code;
    private String shortUrl;
}
