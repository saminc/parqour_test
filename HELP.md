[Тестовое задание](https://docs.google.com/document/d/1zRRqvGzCxLQJoB8IuzhanT1FNkyyL42EMJISpMSDStM/edit)

Основные используемые технологии/инструменты
* Spring boot
* Spring Data JPA
* liquibase
* H2
* Thymeleaf (оформление Bootstrap)
* Lombok

Конфигурирование в application.properties
* при необходимости указать путь для БД (по умолчанию C:/Temp/database/parqour)


Визуально можно посмотреть преобразование по адресу http://localhost:8080/

Логи создания кодов http://localhost:8080/log или пройти в меню



REST API (для получение короткого url и кода)
* URL: http://localhost:8080/shrink 
* Тип запроса: POST
* ПАРАМЕТР {url}, ТИП {String}
* Пример ответа:

```javascript 
{
	"errorCode": null,
	"errorMessage": null,
	"resultData": {
		"code": "RIUhir",
		"shortUrl": "http://localhost:8080/RIUhir"
	}
}
```
URL для короткого URL
* http://localhost:8080/{код}

